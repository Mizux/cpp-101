# Dockerfile for CMake/C++ Development Environment
Currently it build environment for three distribution.

## Ubuntu 

From [ubuntu:latest](https://hub.docker.com/_/ubuntu/):
* [build-essential](http://packages.ubuntu.com/zesty/build-essential)
* [cmake](http://packages.ubuntu.com/zesty/cmake)
* [gcc](http://packages.ubuntu.com/zesty/gcc)
* [clang](http://packages.ubuntu.com/zesty/clang)
* [doxygen](http://packages.ubuntu.com/zesty/doxygen)
* [graphviz](http://packages.ubuntu.com/zesty/graphviz)

## Archlinux Image

From [archlinux:latest](https://hub.docker.com/r/base/archlinux/)
* [base-devel](https://www.archlinux.org/groups/i686/base-devel/)
* [cmake](https://www.archlinux.org/packages/extra/x86_64/cmake/)
* [make](https://www.archlinux.org/packages/core/x86_64/make/)
* [gcc](https://www.archlinux.org/packages/core/x86_64/gcc/)
* [clang](https://www.archlinux.org/packages/extra/x86_64/clang/)
* [doxygen](https://www.archlinux.org/packages/extra/x86_64/doxygen/)
* [graphviz](https://www.archlinux.org/packages/extra/x86_64/graphviz/)

## Alpine Image

From [alpine:latest](https://hub.docker.com/r/alpine/)
* [cmake](https://pkgs.alpinelinux.org/package/edge/main/x86_64/cmake)
* [make](https://pkgs.alpinelinux.org/package/edge/main/x86_64/make)
* [gcc](https://pkgs.alpinelinux.org/package/edge/main/x86_64/gcc)
* [clang](https://pkgs.alpinelinux.org/package/edge/main/x86_64/clang)
* [doxygen](https://pkgs.alpinelinux.org/package/edge/main/x86_64/doxygen)
* [graphviz](https://pkgs.alpinelinux.org/package/edge/main/x86_64/graphviz)

## Manual Build
You can build an image using:
```sh
cd docker/<distro>
docker build -t mizux/cpp/<distro> .
```

## Run
Then you can test the image using:
```sh
docker run --init --rm -it mizux/cpp/<distro> bash
```

## Publish
```sh
docker push mizux/cpp/<distro>
```

## Annexes
To test CI job locally:
```sh
gitlab-runner exec docker --docker-privileged docker:alpine
gitlab-runner exec docker --docker-privileged docker:archlinux
gitlab-runner exec docker --docker-privileged docker:ubuntu
```
