#!/usr/bin/env sh

set -x
apk add --no-cache \
	cmake make gcc clang doxygen graphviz

