#!/usr/bin/env bash

set -x
pacman -Sy --needed --noconfirm \
	cmake make gcc clang doxygen graphviz

