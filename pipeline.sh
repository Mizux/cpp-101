#! /usr/bin/env bash

set -x
gitlab-runner exec docker --docker-privileged docker:alpine
gitlab-runner exec docker --docker-privileged docker:archlinux
gitlab-runner exec docker --docker-privileged docker:ubuntu
