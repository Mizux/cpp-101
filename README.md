devel: [![build status](https://gitlab.com/Mizux/cpp-101/badges/devel/build.svg)](https://gitlab.com/Mizux/cpp-101/commits/devel)  
master: [![build status](https://gitlab.com/Mizux/cpp-101/badges/master/build.svg)](https://gitlab.com/Mizux/cpp-101/commits/master)
# Introduction
Some samples about CMake/C++

# Docker
This repository contains several dockers for having a build environment.  
More info [here](docker/README.md)

# Sample
*ToDo*
